﻿using DG.Tweening;
using UnityEngine;

public class ButtonTween : MonoBehaviour
{
   [SerializeField] private BaseUIButton uiButton;
   [SerializeField] private Vector3 scale = new Vector3(1.2f,1.2f, 1f);
   [SerializeField] private Ease easeIn;
   [SerializeField] private Ease easeOut;
   [SerializeField] private float time;
   
   private Tween tweenButton;

   private void OnEnable()
   {
      uiButton.OnPress += TweenIn;
      uiButton.OnUp += TweenOut;
   }

   private void OnDisable()
   {
      uiButton.OnPress -= TweenIn;
      uiButton.OnUp -= TweenOut;
   }

   private void TweenIn()
   {
      tweenButton?.Kill();
      tweenButton = transform.DOScale(scale, time)
         .SetEase(easeIn);
   }

   private void TweenOut()
   {
      tweenButton?.Kill();
      tweenButton = transform.DOScale(Vector3.one, time)
         .SetEase(easeOut);
   }
}

﻿using UnityEngine;

public class BaseUIGroupButtonOpen : BaseUIButton
{
    [SerializeField] private BaseUIGroup baseUIGroup;
    [SerializeField] private bool _isClosing;

    public override void OnClick()
    {
        if (_isClosing)
        {
            baseUIGroup.Hide();
        }
        else
        {
            baseUIGroup.Show();
        }
    }
}

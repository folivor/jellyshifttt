﻿using System;
using UnityEngine;

[RequireComponent(typeof(CanvasGroup))]
public class BaseUIGroup : MonoBehaviour
{
    public event Action OnShow = () => { };
    public event Action OnHide = () => { };
    [SerializeField] protected CanvasGroup canvasGroup;

    public virtual void Show()
    {
        gameObject.SetActive(true);
    }

    public virtual void Hide()
    {
        gameObject.SetActive(false);
    }

    private void Reset()
    {
        canvasGroup = GetComponent<CanvasGroup>();
    }
}

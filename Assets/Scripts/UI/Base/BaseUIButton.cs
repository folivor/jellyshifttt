﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public abstract class BaseUIButton : MonoBehaviour, IPointerDownHandler, IPointerExitHandler, IPointerUpHandler
{
    public event Action OnPress = () => { };
    public event Action OnUp = () => { };

    private bool isPressed;
    
    public abstract void OnClick();

    private void PressButton()
    {
        if (isPressed)
            return;
        isPressed = true;
        OnPress();
    }

    private void UpButton()
    {
       OnUp();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
       PressButton();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (!isPressed)
            return;
        isPressed = false;
       UpButton();
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (!isPressed)
            return;
        isPressed = false;
       UpButton();
       OnClick();
    }
}

﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelLoadButton : BaseUIButton
{
    [SerializeField] private string lavelName;
    
    public override void OnClick()
    {
        SceneManager.LoadScene(lavelName);
    }
}

﻿using UnityEngine;
using DG.Tweening;

public class FinishScreen : BaseUIGroup
{
    [SerializeField] private Transform buttonReload;

    public override void Show()
    {
        base.Show();
        canvasGroup.alpha = 0f;
        canvasGroup.interactable = false;
        buttonReload.localScale = Vector3.zero;

        canvasGroup.DOFade(1f, 0.5f)
            .OnComplete(() =>
            {
                buttonReload.DOScale(Vector3.one, 0.5f)
                    .SetEase(Ease.OutBack)
                    .OnComplete(() => { canvasGroup.interactable = true;});
            });
    }
}

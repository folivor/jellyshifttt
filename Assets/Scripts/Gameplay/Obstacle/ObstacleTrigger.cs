﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ObstacleTrigger : MonoBehaviour
{
    [SerializeField] private float perfectValue;
    [SerializeField] private Collider trigger;
    [SerializeField] private List<ObstacleBlock> obstacleBlocks;
    [SerializeField] private ObstacleCompleteEffect obstacleCompleteEffect;

    private void OnTriggerEnter(Collider other)
    {
        var player = other.GetComponent<PlayerController>();
        if (player == null)
            return;
        obstacleCompleteEffect.PlayEffect();
        if (Mathf.Abs(perfectValue - player.MorphValue) < 0.1f)
        {
            GameController.Instance.PerfectCompleteObstacle();
        }
    }

    public void DestroyObject()
    {
        trigger.enabled = false;
        obstacleCompleteEffect.PlayEffect();
        obstacleBlocks.ForEach(a=>a.DestroyObject());
    }

    [ContextMenu("Set up component")]
    private void SetUpComponent()
    {
        obstacleBlocks = GetComponentsInChildren<ObstacleBlock>().ToList();
    }
}

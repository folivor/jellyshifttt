﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class ObstacleBlock : MonoBehaviour
{
    [SerializeField] private Rigidbody rigidbody;
    [SerializeField] private float force = 3f;
    [SerializeField] private float forceTorque = 5f;

    private bool isDestroyed;
    
    private void OnCollisionEnter(Collision other)
    {
        if (other.collider.isTrigger)
            return;
        DestroyObject();
    }

    public void DestroyObject()
    {
        if (isDestroyed)
            return;
        isDestroyed = true;
        StartCoroutine(DestroyBlockCorountine());
    }

    private IEnumerator DestroyBlockCorountine()
    {
        rigidbody.useGravity = true;
        gameObject.layer = LayerMask.NameToLayer("IgnoredObstacle");
        rigidbody.AddRelativeTorque(Random.onUnitSphere * forceTorque, ForceMode.Impulse);
        rigidbody.AddForce(Random.onUnitSphere * force, ForceMode.Impulse);

        do
        {
            yield return new WaitForSeconds(4f);
        } 
        while (Mathf.Abs(rigidbody.velocity.sqrMagnitude) > Mathf.Epsilon);

        rigidbody.isKinematic = true;
    }

    private void Reset()
    {
        rigidbody = GetComponent<Rigidbody>();
    }
}

﻿using DG.Tweening;
using UnityEngine;

public class ObstacleCompleteEffect : MonoBehaviour
{
    [SerializeField] private Material transparentObstacleMaterial;
    [SerializeField] private Vector3 size;
    [SerializeField] private float startAlpha;

    public void PlayEffect()
    {
        gameObject.SetActive(true);
        var color = transparentObstacleMaterial.color;
        color.a = startAlpha;
        transparentObstacleMaterial.color = color;
        DOTween.To(x =>
            {
                color.a = Mathf.Lerp(startAlpha, 0f, x);
                transparentObstacleMaterial.color = color;
                transform.localScale = Vector3.Lerp(Vector3.one, size, x);
            }, 0f, 1f, 0.5f)
            .OnComplete(() => gameObject.SetActive(false));
    }
}

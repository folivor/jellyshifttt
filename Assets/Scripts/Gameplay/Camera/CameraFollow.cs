﻿using DG.Tweening;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
   [SerializeField] private PlayerController followTarget;
   [SerializeField] private Vector3 idleRotation;
   [SerializeField] private Vector3 moveRotation;
   [SerializeField] private Transform cameraParent;
   [SerializeField] private Camera camera;

   private Vector3 offset;
   private Vector3 additionalOffset;
   private Vector3 lastRotation;

   private void OnEnable()
   {
      GameController.Instance.OnStartGame += StartMoving;
      GameController.Instance.OnWinGame += EndMoving;
      GameController.Instance.OnBonusActivate += StartSpeedEffect;
      GameController.Instance.OnBonusDiactivate += EndSpeedEffect;
   }

   private void OnDisable()
   {
      if (GameController.Instance == null)
         return;
      GameController.Instance.OnStartGame -= StartMoving;
      GameController.Instance.OnWinGame -= EndMoving;
      GameController.Instance.OnBonusActivate -= StartSpeedEffect;
      GameController.Instance.OnBonusDiactivate -= EndSpeedEffect;
   }

   private void Start()
   {
      offset = transform.position - followTarget.transform.position;
      cameraParent.localRotation = Quaternion.Euler(idleRotation);
   }

   private void StartMoving()
   {
      cameraParent.DOLocalRotate(moveRotation, 0.7f, RotateMode.Fast)
         .SetUpdate(UpdateType.Late);
   }

   private void EndMoving()
   {
      cameraParent.DOLocalRotate(idleRotation, 0.7f, RotateMode.Fast)
         .SetUpdate(UpdateType.Late);
   }

   private void LateUpdate()
   {
      var newPos = followTarget.transform.position;
      newPos.y = transform.position.y;
      transform.position = newPos + offset;
      
      var rotation = followTarget.transform.rotation.eulerAngles;
      if (followTarget.IsMove)
      {
         rotation = transform.rotation.eulerAngles + rotation - lastRotation;
         transform.rotation = Quaternion.Euler(rotation);
      }
      lastRotation = followTarget.transform.rotation.eulerAngles;
   }

   private void StartSpeedEffect()
   {
      DOTween.To(x =>
         {
            camera.fieldOfView = Mathf.Lerp(60f, 70f, x);
         }, 0f, 1f, 0.5f)
         .SetEase(Ease.OutSine);
   }

   private void EndSpeedEffect()
   {
      DOTween.To(x =>
      {
         camera.fieldOfView = Mathf.Lerp(70f, 60f, x);
      }, 0f, 1f, 0.5f);
   }
}

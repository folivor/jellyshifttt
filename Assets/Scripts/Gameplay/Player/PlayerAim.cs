﻿using UnityEngine;

public class PlayerAim : MonoBehaviour
{
    
    [SerializeField] private Transform raycastOutPoint;
    [SerializeField] private Transform aimTransform;
    [SerializeField] private LayerMask layerMask;

    private bool isAiming;
    
    public void EnableAiming()
    {
        isAiming = true;
    }

    public void DisableAiming()
    {
        isAiming = false;
        aimTransform.gameObject.SetActive(false);
    }

    private void FixedUpdate()
    {
        if (!isAiming)
            return;
        RaycastHit hit;
        if (Physics.Raycast(new Ray(raycastOutPoint.position, transform.forward), out hit, Mathf.Infinity, layerMask))
        {
            if (!aimTransform.gameObject.activeSelf)
                aimTransform.gameObject.SetActive(true);

            var newPos = hit.point;
            newPos.y = 0;
            aimTransform.position = newPos;
        }
        else
        {
            aimTransform.gameObject.SetActive(false);
        }
    }
}

﻿using System;
using System.Collections;
using DG.Tweening;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public event Action<float> OnMorphUpdate = x => { }; 
    
    [Header("Dependencies")]
    [SerializeField] private Rigidbody rigidbody; 
    [SerializeField] private PlayerAim playerAim;

    [Header("Parameters")]
    [SerializeField] private float speed = 10f;
    [SerializeField] private float speedBonus = 15f;
   
   [Header("Parameters Animations")]
   [SerializeField] private float startAnimationDuration = 1f;
   [SerializeField] private float winAnimationDuration = 1.5f;
   [SerializeField] private float winJumpDistance;
   [SerializeField] private float winJumpHeight;
   [SerializeField] private float backJumpDistance = 1f;
   [SerializeField] private float backJumpTime = 0.7f;
   [SerializeField] private float backJumpHeight = 1f;

   private bool isMove = false;
   private bool isTouchedObject = false;
   private bool isBonusSpeed = false;
   private float morphValue = 0.5f;

   public bool IsMove => isMove;

   private float currentSpeed => isBonusSpeed ? speedBonus : speed;

   public float MorphValue
   {
       get => morphValue;
       set
       {
           morphValue = Mathf.Clamp01(value);
           OnMorphUpdate(morphValue);
       }
   }

   #region Initializing
    
   private void OnEnable()
   {
       GameController.Instance.OnStartGame += StartGame;
       GameController.Instance.OnWinGame += WinGame;
       GameController.Instance.OnBonusActivate += EnableBonusSpeed;
       GameController.Instance.OnBonusDiactivate += DisableBonusSpeed;
   }

   private void OnDisable()
   {
       if (GameController.Instance == null)
           return;
       GameController.Instance.OnStartGame -= StartGame;
       GameController.Instance.OnWinGame -= WinGame;
       GameController.Instance.OnBonusActivate -= EnableBonusSpeed;
       GameController.Instance.OnBonusDiactivate -= DisableBonusSpeed;
   }

   #endregion

   #region BaseLogic

   private void StartMovement()
   {
       isMove = true;
       rigidbody.isKinematic = false;
   }

   private void StopMovement()
   {
       isMove = false;
       rigidbody.isKinematic = true;
   }
   
   private void StartGame()
   {
       StartCoroutine(StartAnimation(()=>
       {
           StartMovement();
           playerAim.EnableAiming();
       }));
   }

   private void WinGame()
   {
       playerAim.DisableAiming();
       rigidbody.isKinematic = false;
       isMove = false;
       StartCoroutine(WinAnimation());
   }

   private void EnableBonusSpeed()
   {
       isBonusSpeed = true;
   }

   private void DisableBonusSpeed()
   {
       isBonusSpeed = false;
   }

   private void TouchObstacle()
   {
       StopMovement();
       StartCoroutine(BackJump(() =>
       {
           isTouchedObject = false;
           StartMovement();
       }));
   }

   private void FixedUpdate()
   {
       if (isMove)
           Move();
   }

   private void Move()
   {
       var newVelocity = transform.forward * currentSpeed;
       newVelocity.y = rigidbody.velocity.y;
       rigidbody.velocity = newVelocity;
   }

   private void OnCollisionEnter(Collision other)
   {
       if (isTouchedObject || other.collider.isTrigger)
           return;

       if (other.gameObject.GetComponent<ObstacleBlock>() != null)
       {
           TouchObstacle();
           GameController.Instance.ResetBonusCounter();
       }
   }
   
   #endregion
   
   #region Animation

   private IEnumerator StartAnimation(Action callback)
   {
       //Rotate
       transform.DOLocalRotate(Vector3.down * 180f, startAnimationDuration, RotateMode.LocalAxisAdd)
           .SetEase(Ease.OutBack);
       yield return new WaitForSeconds(startAnimationDuration);
       callback?.Invoke();
   }

   private IEnumerator WinAnimation()
   {
       var prepareJumpTime = winAnimationDuration * 0.25f;
       var jumpTime = winAnimationDuration * 0.50f;
       var endJumpTime = winAnimationDuration * 0.25f;

       //Prepare to jump
       transform.DOMove(transform.position + transform.forward * (speed * prepareJumpTime), prepareJumpTime)
           .SetEase(Ease.Linear);
       DOTween.To(x => { MorphValue = x; }, MorphValue, 0f, prepareJumpTime/2f);
       
       yield return new WaitForSeconds(prepareJumpTime);

       var finishPoint = transform.position + transform.forward * winJumpDistance;

       //Jump
       DOTween.To(x => { MorphValue = x; }, MorphValue, 0.5f, jumpTime / 2f)
           .SetEase(Ease.OutBack);
       transform.DOMove(finishPoint, jumpTime)
           .SetEase(Ease.Linear);
       transform.DOLocalMoveY(transform.localPosition.y + winJumpHeight, jumpTime / 2f)
           .SetEase(Ease.OutSine)
           .SetLoops(2, LoopType.Yoyo);
       transform.DOLocalRotate(Vector3.right * 360f, jumpTime, RotateMode.LocalAxisAdd)
           .SetEase(Ease.OutSine);

       yield return new WaitForSeconds(jumpTime);

       //Bounce and rotate
       transform.DOMoveY(transform.position.y + winJumpHeight * 0.1f, endJumpTime / 2f)
           .SetEase(Ease.OutCirc)
           .SetLoops(2, LoopType.Yoyo);
       transform.DOLocalRotate(Vector3.up * 180f, endJumpTime, RotateMode.LocalAxisAdd)
           .SetEase(Ease.OutSine);
       
       yield return new WaitForSeconds(endJumpTime);
   }

   private IEnumerator BackJump(Action callback)
   {
       transform.DOMove(transform.position - transform.forward * backJumpDistance, backJumpTime)
           .SetEase(Ease.Linear);
       transform.DOLocalMoveY(transform.localPosition.y + backJumpHeight, backJumpTime / 2f)
           .SetEase(Ease.OutSine)
           .SetLoops(2, LoopType.Yoyo);
       yield return new WaitForSeconds(backJumpTime);
       callback?.Invoke();
   }

   #endregion
}

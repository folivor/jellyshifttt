﻿using UnityEngine;

public class DestroyerObstacle : MonoBehaviour
{
    [SerializeField] private Collider trigger;

    private void OnEnable()
    {
        GameController.Instance.OnBonusActivate += ActivateTrigger;
        GameController.Instance.OnBonusDiactivate += DiactivateTrigger;
    }

    private void OnDisable()
    {
        if (GameController.Instance == null)
            return;
        GameController.Instance.OnBonusActivate -= ActivateTrigger;
        GameController.Instance.OnBonusDiactivate -= DiactivateTrigger;
    }

    private void ActivateTrigger()
    {
        trigger.enabled = true;
    }

    private void DiactivateTrigger()
    {
        trigger.enabled = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        var obstacle = other.GetComponent<ObstacleTrigger>();
        if (obstacle != null)
            obstacle.DestroyObject();
    }
}

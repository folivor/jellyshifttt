﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using Random = UnityEngine.Random;

public class WorldColorController : MonoBehaviour
{
	[SerializeField] private Material skyboxMaterial;
	[SerializeField] private List<SkyboxColorSet> skyboxColorsSets;
	[SerializeField] private SkyboxColorSet comboColorSet;
	[SerializeField] private float timeChangeColor;

	private SkyboxColorSet currentColorSet;

	private Tween tweenSkyColor;
	private Coroutine changingColorCorountine;

	private void OnEnable()
	{
		GameController.Instance.OnBonusActivate += StartComboBonus;
		GameController.Instance.OnBonusDiactivate += EndColorBonus;
	}

	private void OnDisable()
	{
		if (GameController.Instance == null)
			return;
		GameController.Instance.OnBonusActivate -= StartComboBonus;
		GameController.Instance.OnBonusDiactivate -= EndColorBonus;
	}

	private void Start()
	{
		currentColorSet = skyboxColorsSets[Random.Range(0, skyboxColorsSets.Count)];
		SetColorSkybox(currentColorSet.topColor, currentColorSet.midColor, currentColorSet.botColor);
		changingColorCorountine = StartCoroutine(ChangingColor());
	}

	private IEnumerator ChangingColor()
	{
		while (true)
		{
			yield return new WaitForSeconds(timeChangeColor);
			var listColors = skyboxColorsSets.FindAll(a => a != currentColorSet);
			var nextColorSet = listColors[Random.Range(0, listColors.Count)];
			currentColorSet = nextColorSet;
			ChangeSkyboxToColor(nextColorSet);
		}
	}

	private void StartComboBonus()
	{
		StopCoroutine(changingColorCorountine);
		ChangeSkyboxToColor(comboColorSet, 1f);
	}

	private void EndColorBonus()
	{
		currentColorSet = skyboxColorsSets[Random.Range(0, skyboxColorsSets.Count)];
		ChangeSkyboxToColor(currentColorSet, 1f);
		changingColorCorountine = StartCoroutine(ChangingColor());
	}

	public void Update()
	{
		if (Input.GetKeyDown(KeyCode.LeftShift))
		{
			StartComboBonus();
		}

		if (Input.GetKeyDown(KeyCode.Space))
		{
			EndColorBonus();
		}
	}

	private void ChangeSkyboxToColor(SkyboxColorSet colorSet, float timeChange = 3f)
	{
		tweenSkyColor?.Kill();
		var topColor = skyboxMaterial.GetColor("_ColorTop");
		var midColor = skyboxMaterial.GetColor("_ColorMid");
		var botColor = skyboxMaterial.GetColor("_ColorBot");

		tweenSkyColor = DOTween.To(x =>
		{
			var newTopColor = Color.Lerp(topColor, colorSet.topColor, x);
			var newMidColor = Color.Lerp(midColor, colorSet.midColor, x);
			var newBotColor = Color.Lerp(botColor, colorSet.botColor, x);
			SetColorSkybox(newTopColor, newMidColor, newBotColor);
		}, 0f, 1f, timeChange);
	}

	private void SetColorSkybox(Color topColor, Color midColor, Color botColor)
	{
		skyboxMaterial.SetColor("_ColorTop", topColor);
		skyboxMaterial.SetColor("_ColorMid", midColor);
		skyboxMaterial.SetColor("_ColorBot", botColor);
	}
}

[Serializable]
public class SkyboxColorSet
{
	public Color topColor;
	public Color midColor;
	public Color botColor;
}
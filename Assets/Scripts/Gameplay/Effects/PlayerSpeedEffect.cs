﻿using UnityEngine;

public class PlayerSpeedEffect : MonoBehaviour
{
    [SerializeField] private ParticleSystem speedEffect;

    private void OnEnable()
    {
        GameController.Instance.OnBonusActivate += EnableEffect;
        GameController.Instance.OnBonusDiactivate += DisableEffect;
        GameController.Instance.OnWinGame += DisableEffect;
    }

    private void OnDisable()
    {
        if (GameController.Instance == null)
            return;
        GameController.Instance.OnBonusActivate -= EnableEffect;
        GameController.Instance.OnBonusDiactivate -= DisableEffect;
        GameController.Instance.OnWinGame -= DisableEffect;
    }

    private void EnableEffect()
    {
        speedEffect.Play();
    }

    private void DisableEffect()
    {
        speedEffect.Stop();
    }
}

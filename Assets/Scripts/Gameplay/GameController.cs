﻿using System;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public static GameController Instance;
    
    public event Action OnStartGame = () => { };
    public event Action OnWinGame = () => { };
    public event Action OnLoseGame = () => { };
    public event Action OnBonusActivate = () => { };
    public event Action OnBonusDiactivate = () => { };

    [SerializeField] private FinishScreen finishScreen;
    [SerializeField] private int comboBonus = 5;
    [SerializeField] private int comboBonusLenght = 5;
    
    private bool _isGameActive = false;

    public bool IsGameActive => _isGameActive;

    private int comboCounter = 0;

    private void Awake()
    {
        Instance = this;
    }

    private void OnDestroy()
    {
        Instance = null;
    }

    private void Start()
    {
        Invoke(nameof(StartGame), 3f);
    }

    public void StartGame()
    {
        if (_isGameActive)
            return;
        _isGameActive = true;
        OnStartGame();
    }

    public void WinGame()
    {
        if (!_isGameActive)
            return;
        _isGameActive = false;
        OnWinGame();
        Invoke("ShowFinishScreen", 3f);
    }

    public void LoseGame()
    {
        if (!_isGameActive)
            return;
        _isGameActive = false;
        OnLoseGame();
    }

    public void PerfectCompleteObstacle()
    {
        comboCounter++;
        if (comboBonus == comboCounter)
        {
            StartBonus();
            comboCounter = 0;
        }
    }

    public void ResetBonusCounter()
    {
        comboCounter = 0;
    }

    private void StartBonus()
    {
        OnBonusActivate();
        Invoke(nameof(EndBonus), comboBonusLenght);
    }

    private void EndBonus()
    {
        OnBonusDiactivate();
    }

    private void ShowFinishScreen()
    {
        finishScreen.Show();
    }
}
﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlayerControl : MonoBehaviour, IPointerDownHandler, IPointerExitHandler, IPointerUpHandler
{
    [SerializeField] private float maxDragLenght;

    [SerializeField] private PlayerController playerController;

    private float startPlayerMorfing;

    private float startPointerPos;

    private bool isDrag;
    
    public void OnPointerDown(PointerEventData eventData)
    {
        if (isDrag)
            return;
        isDrag = true;
        startPointerPos = Input.mousePosition.y;
        startPlayerMorfing = playerController.MorphValue;
    }

    private void Update()
    {
        if (!isDrag || !GameController.Instance.IsGameActive)
            return;
        var delta = Input.mousePosition.y - startPointerPos;
        playerController.MorphValue = startPlayerMorfing + (delta / maxDragLenght);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (!isDrag)
            return;
        isDrag = false;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (!isDrag)
            return;
        isDrag = false;
    }
}

﻿using UnityEngine;

public class RotatingTrigger : BaseGameTrigger
{
   [SerializeField] private Transform fromPoint;
   [SerializeField] private Transform toPoint;

   private Vector3 projectionFinishPoint;
   private float projectionMagnitute;
   private bool isRotating;
   private PlayerController player;

   public float magnitude;

   private void Start()
   {
      var distanceVector = fromPoint.position - toPoint.position;
      projectionFinishPoint = Vector3.Project(distanceVector, fromPoint.forward);
      projectionMagnitute = projectionFinishPoint.magnitude;
      magnitude = projectionMagnitute;
   }

   //Calculate projection player positon on start forward vector
   private void FixedUpdate()
   {
      if (!isRotating)
         return;

      var distanceVector = player.transform.position - fromPoint.position;
      
      if (Vector3.Dot(distanceVector, fromPoint.forward) < 0)
         return;
      
      var playerLerpParameter = 
         Vector3.Project(distanceVector, fromPoint.forward).magnitude;
      
      playerLerpParameter = playerLerpParameter / projectionMagnitute;

      Debug.Log(playerLerpParameter);
      
      if (playerLerpParameter >= 0.99f)
      {
         playerLerpParameter = 1f;
         isRotating = false;
      }
      
      RotatePlayer(playerLerpParameter);
   }

   //Change player forward vector between two forward vectors by lerp
   private void RotatePlayer(float t)
   {
      player.transform.forward =
         Vector3.Slerp(fromPoint.forward, toPoint.forward, t*t);
   }

   protected override void OnPlayerDetect(PlayerController player)
   {
      this.player = player;
      isRotating = true;
   }
}

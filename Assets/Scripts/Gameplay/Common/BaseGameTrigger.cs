﻿using UnityEngine;

public abstract class BaseGameTrigger : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.isTrigger)
            return;
        var player = other.GetComponent<PlayerController>();
        if (player != null)
        {
            OnPlayerDetect(player);
        }
    }

    protected abstract void OnPlayerDetect(PlayerController player);
}

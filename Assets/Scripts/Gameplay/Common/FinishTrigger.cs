﻿public class FinishTrigger : BaseGameTrigger
{
    protected override void OnPlayerDetect(PlayerController player)
    {
        GameController.Instance.WinGame();
    }
}

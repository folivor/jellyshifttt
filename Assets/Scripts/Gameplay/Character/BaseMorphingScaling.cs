﻿using UnityEngine;

public class BaseMorphingScaling : MonoBehaviour
{
    [SerializeField] private PlayerController playerController;

    private void OnEnable()
    {
        playerController.OnMorphUpdate += ChangeMorphing;
    }

    private void OnDisable()
    {
        playerController.OnMorphUpdate -= ChangeMorphing;
    }

    private void Start()
    {
        ChangeMorphing(playerController.MorphValue);
    }

    protected virtual void ChangeMorphing(float value)
    {
    }
}
﻿using UnityEngine;

public class ColliderScaling : BaseMorphingScaling
{
    [SerializeField] private BoxCollider boxCollider;
    [SerializeField] private Vector3 highState;
    [SerializeField] private Vector3 middleState;
    [SerializeField] private Vector3 lowState;

    protected override void ChangeMorphing(float value)
    {
        var toScale = (value > 0.5f) ? highState : lowState;
        var lerpArgument = (value > 0.5f) ? (value - 0.5f) * 2f : 1f - value * 2f;
        toScale = Vector3.Lerp(middleState, toScale, lerpArgument);
        var offset = toScale.y - boxCollider.size.y;
        boxCollider.size = toScale;
        boxCollider.center += Vector3.up * (offset / 2f);
    }
}

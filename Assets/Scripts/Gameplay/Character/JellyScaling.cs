﻿using UnityEngine;

public class JellyScaling : BaseMorphingScaling
{
   [SerializeField] private Vector3 highState;
   [SerializeField] private Vector3 lowState;
   
   protected override void ChangeMorphing(float value)
   {
      var toScale = (value > 0.5f) ? highState : lowState;
      var lerpArgument = (value > 0.5f) ? (value - 0.5f) * 2f : 1f - value * 2f;
      toScale = Vector3.Lerp(Vector3.one, toScale, lerpArgument);
      transform.localScale = toScale;
   }

}
